import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColoritoComponent } from './colorito.component';

describe('ColoritoComponent', () => {
  let component: ColoritoComponent;
  let fixture: ComponentFixture<ColoritoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColoritoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColoritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
