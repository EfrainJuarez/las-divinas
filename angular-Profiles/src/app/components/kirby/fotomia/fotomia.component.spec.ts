import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FotomiaComponent } from './fotomia.component';

describe('FotomiaComponent', () => {
  let component: FotomiaComponent;
  let fixture: ComponentFixture<FotomiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FotomiaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FotomiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
