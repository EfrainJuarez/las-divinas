import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonajefavComponent } from './personajefav.component';

describe('PersonajefavComponent', () => {
  let component: PersonajefavComponent;
  let fixture: ComponentFixture<PersonajefavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonajefavComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonajefavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
