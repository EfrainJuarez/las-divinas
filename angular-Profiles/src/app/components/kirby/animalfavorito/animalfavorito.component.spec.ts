import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalfavoritoComponent } from './animalfavorito.component';

describe('AnimalfavoritoComponent', () => {
  let component: AnimalfavoritoComponent;
  let fixture: ComponentFixture<AnimalfavoritoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnimalfavoritoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AnimalfavoritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
