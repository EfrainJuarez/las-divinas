import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TacofavoritoComponent } from './tacofavorito.component';

describe('TacofavoritoComponent', () => {
  let component: TacofavoritoComponent;
  let fixture: ComponentFixture<TacofavoritoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TacofavoritoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TacofavoritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
