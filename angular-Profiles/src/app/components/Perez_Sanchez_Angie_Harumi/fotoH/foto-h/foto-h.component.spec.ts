import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FotoHComponent } from './foto-h.component';

describe('FotoHComponent', () => {
  let component: FotoHComponent;
  let fixture: ComponentFixture<FotoHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FotoHComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FotoHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
