import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NombreHComponent } from './nombre-h.component';

describe('NombreHComponent', () => {
  let component: NombreHComponent;
  let fixture: ComponentFixture<NombreHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NombreHComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NombreHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
