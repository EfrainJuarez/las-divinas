import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComidaHComponent } from './comida-h.component';

describe('ComidaHComponent', () => {
  let component: ComidaHComponent;
  let fixture: ComponentFixture<ComidaHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComidaHComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComidaHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
