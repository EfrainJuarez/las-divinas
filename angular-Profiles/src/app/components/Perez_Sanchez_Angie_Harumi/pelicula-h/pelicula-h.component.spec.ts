import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeliculaHComponent } from './pelicula-h.component';

describe('PeliculaHComponent', () => {
  let component: PeliculaHComponent;
  let fixture: ComponentFixture<PeliculaHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PeliculaHComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PeliculaHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
