import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BandaHComponent } from './banda-h.component';

describe('BandaHComponent', () => {
  let component: BandaHComponent;
  let fixture: ComponentFixture<BandaHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BandaHComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BandaHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
