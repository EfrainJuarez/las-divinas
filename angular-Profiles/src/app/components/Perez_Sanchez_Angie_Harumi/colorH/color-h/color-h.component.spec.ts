import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorHComponent } from './color-h.component';

describe('ColorHComponent', () => {
  let component: ColorHComponent;
  let fixture: ComponentFixture<ColorHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorHComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColorHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
