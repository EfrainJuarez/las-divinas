import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorRComponent } from './color-r.component';

describe('ColorRComponent', () => {
  let component: ColorRComponent;
  let fixture: ComponentFixture<ColorRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorRComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColorRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
