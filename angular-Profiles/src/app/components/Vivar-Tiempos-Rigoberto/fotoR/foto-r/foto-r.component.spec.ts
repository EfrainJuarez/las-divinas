import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FotoRComponent } from './foto-r.component';

describe('FotoRComponent', () => {
  let component: FotoRComponent;
  let fixture: ComponentFixture<FotoRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FotoRComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FotoRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
