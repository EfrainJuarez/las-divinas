import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiestaRComponent } from './fiesta-r.component';

describe('FiestaRComponent', () => {
  let component: FiestaRComponent;
  let fixture: ComponentFixture<FiestaRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiestaRComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FiestaRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
