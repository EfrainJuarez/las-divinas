import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeliculaRComponent } from './pelicula-r.component';

describe('PeliculaRComponent', () => {
  let component: PeliculaRComponent;
  let fixture: ComponentFixture<PeliculaRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PeliculaRComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PeliculaRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
