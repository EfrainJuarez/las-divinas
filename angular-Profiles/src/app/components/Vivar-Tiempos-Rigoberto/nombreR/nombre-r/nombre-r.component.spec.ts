import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NombreRComponent } from './nombre-r.component';

describe('NombreRComponent', () => {
  let component: NombreRComponent;
  let fixture: ComponentFixture<NombreRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NombreRComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NombreRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
