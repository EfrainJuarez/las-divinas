import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HobbyEJMComponent } from './hobby-ejm.component';

describe('HobbyEJMComponent', () => {
  let component: HobbyEJMComponent;
  let fixture: ComponentFixture<HobbyEJMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HobbyEJMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HobbyEJMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
