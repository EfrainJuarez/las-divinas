import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoEJMComponent } from './photo-ejm.component';

describe('PhotoEJMComponent', () => {
  let component: PhotoEJMComponent;
  let fixture: ComponentFixture<PhotoEJMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhotoEJMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PhotoEJMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
