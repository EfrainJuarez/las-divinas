import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoEJMComponent } from './info-ejm.component';

describe('InfoEJMComponent', () => {
  let component: InfoEJMComponent;
  let fixture: ComponentFixture<InfoEJMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoEJMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InfoEJMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
