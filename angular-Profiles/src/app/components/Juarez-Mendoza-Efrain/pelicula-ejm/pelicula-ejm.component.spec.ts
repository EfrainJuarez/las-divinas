import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeliculaEJMComponent } from './pelicula-ejm.component';

describe('PeliculaEJMComponent', () => {
  let component: PeliculaEJMComponent;
  let fixture: ComponentFixture<PeliculaEJMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PeliculaEJMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PeliculaEJMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
