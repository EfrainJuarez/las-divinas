import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorEJMComponent } from './color-ejm.component';

describe('ColorEJMComponent', () => {
  let component: ColorEJMComponent;
  let fixture: ComponentFixture<ColorEJMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorEJMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColorEJMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
