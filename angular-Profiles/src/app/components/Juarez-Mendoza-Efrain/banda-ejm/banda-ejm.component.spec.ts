import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BandaEJMComponent } from './banda-ejm.component';

describe('BandaEJMComponent', () => {
  let component: BandaEJMComponent;
  let fixture: ComponentFixture<BandaEJMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BandaEJMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BandaEJMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
