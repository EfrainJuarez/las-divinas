import { Component } from '@angular/core';

@Component({
  selector: 'app-cancion',
  templateUrl: './cancion.component.html',
  styleUrls: ['./cancion.component.css']
})
export class CancionComponent {
  constructor(){}
  public song = new Audio("../../../../assets/assetsAlvaro/soda.mp3");

  ngOnInit(): void {
    
  }
  reproducirCancion = () =>{
    let icono = document.querySelector(".btn");
    if (icono?.classList.toggle("reproducir")) {
      icono?.setAttribute("src","../../../../assets/assetsAlvaro/pause.png");
      this.song.play(); 

    }else{
      this.song.pause();
      icono?.setAttribute("src","../../../../assets/assetsAlvaro/play.png");
    }
  }
}
