import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PhotoComponent } from './components/Espindola_Torres_Alvaro_Isaias/photo/photo.component';
import { ColorComponent } from './components/Espindola_Torres_Alvaro_Isaias/color/color.component';
import { CancionComponent } from './components/Espindola_Torres_Alvaro_Isaias/cancion/cancion.component';
import { BandaComponent } from './components/Espindola_Torres_Alvaro_Isaias/banda/banda.component';
import { PeliculaComponent } from './components/Espindola_Torres_Alvaro_Isaias/pelicula/pelicula.component';
import { HobbyComponent } from './components/Espindola_Torres_Alvaro_Isaias/hobby/hobby.component';
import { FotomiaComponent } from './components/kirby/fotomia/fotomia.component';
import { ComidaComponent } from './components/kirby/comida/comida.component';
import { PasatiempoComponent } from './components/kirby/pasatiempo/pasatiempo.component';
import { PersonajefavComponent } from './components/kirby/personajefav/personajefav.component';
import { TacofavoritoComponent } from './components/kirby/tacofavorito/tacofavorito.component';
import { AnimalfavoritoComponent } from './components/kirby/animalfavorito/animalfavorito.component';

import { PhotoEJMComponent } from './components/Juarez-Mendoza-Efrain/photo-ejm/photo-ejm.component';
import { InfoEJMComponent } from './components/Juarez-Mendoza-Efrain/info-ejm/info-ejm.component';
import { ColorEJMComponent } from './components/Juarez-Mendoza-Efrain/color-ejm/color-ejm.component';
import { PeliculaEJMComponent } from './components/Juarez-Mendoza-Efrain/pelicula-ejm/pelicula-ejm.component';
import { HobbyEJMComponent } from './components/Juarez-Mendoza-Efrain/hobby-ejm/hobby-ejm.component';
import { BandaEJMComponent } from './components/Juarez-Mendoza-Efrain/banda-ejm/banda-ejm.component';

import { FotoComponent } from './components/Belaunzaran-Fiorentini-Josel/foto/foto.component';
import { MusicaComponent } from './components/Belaunzaran-Fiorentini-Josel/musica/musica.component';
import { HobbiesComponent } from './components/Belaunzaran-Fiorentini-Josel/hobbies/hobbies.component';
import { AmigosComponent } from './components/Belaunzaran-Fiorentini-Josel/amigos/amigos.component';
import { ColoritoComponent } from './components/Belaunzaran-Fiorentini-Josel/colorito/colorito.component';
import { FotoHComponent } from './components/Perez_Sanchez_Angie_Harumi/fotoH/foto-h/foto-h.component';
import { NombreHComponent } from './components/Perez_Sanchez_Angie_Harumi/nombreH/nombre-h/nombre-h.component';
import { ColorHComponent } from './components/Perez_Sanchez_Angie_Harumi/colorH/color-h/color-h.component';
import { BandaHComponent } from './components/Perez_Sanchez_Angie_Harumi/bandaH/banda-h/banda-h.component';
import { ComidaHComponent } from './components/Perez_Sanchez_Angie_Harumi/comidaH/comida-h/comida-h.component';

import { FiestaRComponent } from './components/Vivar-Tiempos-Rigoberto/fiestaR/fiesta-r/fiesta-r.component';
import { PeliculaRComponent } from './components/Vivar-Tiempos-Rigoberto/peliculaR/pelicula-r/pelicula-r.component';
import { ColorRComponent } from './components/Vivar-Tiempos-Rigoberto/colorR/color-r/color-r.component';
import { FotoRComponent } from './components/Vivar-Tiempos-Rigoberto/fotoR/foto-r/foto-r.component';
import { NombreRComponent } from './components/Vivar-Tiempos-Rigoberto/nombreR/nombre-r/nombre-r.component';
import { PeliculaHComponent } from './components/Perez_Sanchez_Angie_Harumi/pelicula-h/pelicula-h.component';


@NgModule({
  declarations: [
    AppComponent,
    PhotoComponent,
    ColorComponent,
    CancionComponent,
    BandaComponent,
    PeliculaComponent,
    HobbyComponent,
    FotomiaComponent,
    ComidaComponent,
    PasatiempoComponent,
    PersonajefavComponent,
    TacofavoritoComponent,
    AnimalfavoritoComponent,
    PhotoEJMComponent,
    InfoEJMComponent,
    ColorEJMComponent,
    PeliculaEJMComponent,
    HobbyEJMComponent,
    BandaEJMComponent,
    FotoComponent,
    MusicaComponent,
    HobbiesComponent,
    AmigosComponent,
    ColoritoComponent,

    FotoHComponent,
    NombreHComponent,
    ColorHComponent,
    BandaHComponent,
    ComidaHComponent,

    FiestaRComponent,
    PeliculaRComponent,
    ColorRComponent,
    FotoRComponent,
    NombreRComponent,
    PeliculaHComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
