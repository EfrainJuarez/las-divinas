import { Photo } from './components/Espindola_Torres_Alvaro_Isaias/photo/photo';
import { Color } from './components/Espindola_Torres_Alvaro_Isaias/color/color';
import { Cancion } from './components/Espindola_Torres_Alvaro_Isaias/cancion/cancion';
import { Banda } from './components/Espindola_Torres_Alvaro_Isaias/banda/banda';
import { Pelicula } from './components/Espindola_Torres_Alvaro_Isaias/pelicula/pelicula';
import { Hobby } from './components/Espindola_Torres_Alvaro_Isaias/hobby/hobby';
import { Fotomia } from './components/kirby/fotomia/Fotomia';
import { Animalfavorito } from './components/kirby/animalfavorito/Animalfavorito';
import { Comida } from './components/kirby/comida/Comida';
import { Pasatiempo } from './components/kirby/pasatiempo/Pasatiempo';
import { Tacofavorito } from './components/kirby/tacofavorito/Tacofavorito';
import { Personajefav } from './components/kirby/personajefav/Personajefav';

import { Foto } from './components/Belaunzaran-Fiorentini-Josel/foto/foto';
import { Colorito } from './components/Belaunzaran-Fiorentini-Josel/colorito/colorito';
import { Cancioncita } from './components/Belaunzaran-Fiorentini-Josel/cancioncita/cancioncita';
import { Video } from './components/Belaunzaran-Fiorentini-Josel/video/video';
import { Serie } from './components/Belaunzaran-Fiorentini-Josel/serie/serie';


import {PhotoEJM} from './components/Juarez_Mendoza_Efrain/photoEJM/photoEJM';
import {InfoEJM} from './components/Juarez_Mendoza_Efrain/infoEJM/infoEJM';
import {HobbyEJM} from './components/Juarez_Mendoza_Efrain/hobbyEJM/hobbyEJM';
import {PeliculaEJM} from './components/Juarez_Mendoza_Efrain/peliculaEJM/peliculaEJM';
import {ColorEJM} from './components/Juarez_Mendoza_Efrain/colorEJM/colorEJM';
import {BandaEJM} from './components/Juarez_Mendoza_Efrain/bandaEJM/bandaEJM';

import {FiestaR} from './components/Vivar_Tiempos_Rigoberto/fiestaR/fiestaR';
import {ColorR} from './components/Vivar_Tiempos_Rigoberto/colorR/colorR';
import {PeliculaR} from './components/Vivar_Tiempos_Rigoberto/peliculaR/peliculaR';
import { FotoR } from './components/Vivar_Tiempos_Rigoberto/fotoRigo/fotoR';

import {BandaH} from './components/Perez-Sanchez-Angie-Harumi/bandaH/banda';
import {ColorH} from './components/Perez-Sanchez-Angie-Harumi/colorH/color';
import {ComidaH} from './components/Perez-Sanchez-Angie-Harumi/comidaH/comida';
import {FotoH} from './components/Perez-Sanchez-Angie-Harumi/fotoH/foto';
import {PeliculaH} from './components/Perez-Sanchez-Angie-Harumi/peliculaH/peliculaH';

import './App.css'

function App() {

  return (
    <div className="container-principal">
      <div className="container-item bela">
        <Foto></Foto>
        <Colorito></Colorito>
        <Cancioncita></Cancioncita>
        <Video></Video>
        <Serie></Serie>
      </div>

      <div className="container-item alvaro">
        <Photo></Photo>
        <Color></Color>
        <Cancion></Cancion>
        <Banda></Banda>
        <Pelicula></Pelicula>
        <Hobby></Hobby>
      </div>

      <div className="container-item kirbi">
        <Fotomia></Fotomia>
        <Comida></Comida>
        <Tacofavorito></Tacofavorito>
        <Personajefav></Personajefav>
        <Animalfavorito></Animalfavorito>
        <Pasatiempo></Pasatiempo>

      </div>

      <div className="container-item efra">
        <PhotoEJM></PhotoEJM>
        <InfoEJM></InfoEJM>
        <HobbyEJM></HobbyEJM>
        <PeliculaEJM></PeliculaEJM>
        <ColorEJM></ColorEJM>
        <BandaEJM></BandaEJM>
      </div>

      <div className="container-item haru">
        <FotoH></FotoH>
        <BandaH></BandaH>
        <ComidaH></ComidaH>
        <ColorH></ColorH>
        <PeliculaH></PeliculaH>

      </div>

      <div className="container-item rigo">
        <FotoR></FotoR>
        <ColorR></ColorR>
        <FiestaR></FiestaR>
        <PeliculaR></PeliculaR>

      </div>
    </div>
  )
}

export default App
