import albumBanda from '../../../assets/assetsRigo/keanu.jpg';
import './peliculaR.css'

export const PeliculaR = () => {
  return (
    <div className="principal-container-band">
      <div className="banda-name">
        <h2>Pelicula</h2>
      </div>
      <div className="band">
        <img src={albumBanda} alt="" />
      </div>
    </div>
  );
};
