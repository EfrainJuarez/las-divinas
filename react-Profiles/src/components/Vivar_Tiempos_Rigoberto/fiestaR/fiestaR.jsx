import albumBanda from '../../../assets/assetsRigo/nav.jpg';
import './fiestaR.css'

export const FiestaR = () => {
  return (
    <div className="principal-container-band">
      <div className="banda-name">
        <h2>Fiesta Favorita</h2>
      </div>
      <div className="band">
        <img src={albumBanda} alt="" />
      </div>
    </div>
  );
};
