import photo from '../../../assets/assetsRigo/yo.jpg';

import './fotoR.css';

export const FotoR = () => {
  return (
    <div className="container-photo">
      <div className="photo">
        <img src={photo} alt=""/>
      </div>
    </div>
  );
};
