import mando from '../../../assets/assetsBela/mando.png';
import './serie.css';

export const Serie = () => {
  return (
    <div className="container-mando">
      <div className="foto-mando">
        <img src={mando} alt=""/>
      </div>
      <div className="name-mando">
        <h2>Serie </h2>
      </div>
    </div>
  );
};