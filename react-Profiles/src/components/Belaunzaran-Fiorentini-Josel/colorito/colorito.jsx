import verde from '../../../assets/assetsBela/verde.png';
import './colorito.css';

export const Colorito = () => {
  return (
    <div className="container-fotito">
      <div className="foto-color">
        <img src={verde} alt=""/>
      </div>
      <div className="name-color">
        <h3>Verde sable de luz</h3>
      </div>
      
    </div>
  );
};
