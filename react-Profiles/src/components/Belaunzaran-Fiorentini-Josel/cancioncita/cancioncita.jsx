import Journey from '../../../assets/assetsBela/Journey.png';
import './cancioncita.css';

export const Cancioncita = () => {
  return (
    <div className="container-journey">
      <div className="foto-cancion">
        <img src={Journey} alt=""/>
      </div>
      <div className="name-cancion">
        <h2>Journey</h2>
      </div>
    </div>
  );
};
