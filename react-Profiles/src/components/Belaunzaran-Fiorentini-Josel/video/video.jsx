import balon from '../../../assets/assetsBela/balon.png';
import './video.css';

export const Video = () => {
  return (
    <div className="container-balon">
      <div className="foto-balon">
        <img src={balon} alt=""/>
      </div>
      <div className="name-balon">
        <h2>Basket</h2>
      </div>
    </div>
  );
};