import yo from '../../../assets/assetsBela/yo.png';
import './foto.css';

export const Foto = () => {
  return (
    <div className="container-fotito">
      <div className="foto">
        <img src={yo} alt=""/>
      </div>
      <div className="name">
        <h2>Belaunzaran Fiorentini Josel</h2>
      </div>
    </div>
  );
};
