import amongusnalgon from '../../../assets/assetsKirby/amongusnalgon.jpeg';
import './Pasatiempo.css';

export const Pasatiempo = () => {
    return(
        <div className="container-dibujo">
            <div className="letras-dibujo">
                <h3>Pasatiempo</h3>
            </div>
            <div className="dibujo">
                <img src={amongusnalgon} alt="" />
            </div>
        </div>
    )
}