import comida from '../../../assets/assetsKirby/hamburguesa.jpg';
import './Comida.css';

export const Comida = () => {
    return(
        <div className="container-hamburguesa">
            <div className="letras-hamburguesa">
                <h3>Comida favorita</h3>
            </div>
            <div className="hamburguesa">
                <img src={comida} alt="" />
            </div>
        </div>
    )
}