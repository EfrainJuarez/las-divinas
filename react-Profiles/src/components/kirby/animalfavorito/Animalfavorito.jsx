import pochita from '../../../assets/assetsKirby/pochita.jpeg';
import './Animalfavorito.css';

export const Animalfavorito = () => {
    return(
        <div className="container-pochita">
            <div className="letras-pochita">
                <h3>Animal favorito</h3>
            </div>
            <div className="pochita">
                <img src={pochita} alt="" />
            </div>
        </div>
    )
}