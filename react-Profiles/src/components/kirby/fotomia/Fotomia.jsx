import fotomia from '../../../assets/assetsKirby/fotomia.jpeg';
import './Fotomia.css';

export const Fotomia = () => {
    return(
        <div className="container-fotomia">
            <div className="foto">
                <img src={fotomia} alt="" />
            </div>
            <div className="letras-fotomia">
                <h2>Guzmán Gómez Angel Hiramg</h2>
            </div>
        </div>
    )
}