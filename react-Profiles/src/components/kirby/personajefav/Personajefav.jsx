import kirby from '../../../assets/assetsKirby/kirbyaudifonos.jpeg';
import './Personajefav.css';

export const Personajefav = () => {
    return(
        <div className="container-kirby">
            <div className="letras-kirby">
                <h3>Personaje favorito</h3>
            </div>
            <div className="kirby">
                <img src={kirby} alt="" />
            </div>
        </div>
    )
}