import taquito from '../../../assets/assetsKirby/tacomilanesa.jpg';
import './Tacofavorito.css';

export const Tacofavorito = () => {
    return(
        <div className="container-taco">
            <div className="letras-taco">
                <h3>Taco favorito</h3>
            </div>
            <div className="taco">
                <img src={taquito} alt="" />
            </div>
        </div>
    )
}