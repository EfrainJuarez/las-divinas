import albumBanda from '../../../assets/assestsHarumi/pelicula.jpeg';
import './peliculaH.css'

export const PeliculaH = () => {
  return (
    <div className="principal-container-bandr">
      <div className="banda-name">
        <h2>Pelicula</h2>
      </div>
      <div className="bandr">
        <img src={albumBanda} alt="" />
      </div>
    </div>
  );
};
