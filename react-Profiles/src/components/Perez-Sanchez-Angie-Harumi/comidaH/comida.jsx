import albumBanda from '../../../assets/assestsHarumi/comida.jpg';
import './comida.css'

export const ComidaH = () => {
  return (
    <div className="principal-container-bandr">
      <div className="banda-name">
        <h2>Comida</h2>
      </div>
      <div className="bandr">
        <img src={albumBanda} alt="" />
      </div>
    </div>
  );
};
