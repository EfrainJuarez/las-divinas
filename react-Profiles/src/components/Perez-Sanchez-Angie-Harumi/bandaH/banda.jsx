import albumBanda from '../../../assets/assestsHarumi/yo.png';
import './banda.css'

export const BandaH = () => {
  return (
    <div className="principal-container-bandr">
      <div className="banda-name">
        <h2>Grupo</h2>
      </div>
      <div className="bandr">
        <img src={albumBanda} alt="" />
      </div>
    </div>
  );
};
