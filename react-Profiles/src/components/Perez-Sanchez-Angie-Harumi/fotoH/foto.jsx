import yo from '../../../assets/assestsHarumi/angie.jpg';
import './foto.css';

export const FotoH = () => {
  return (
    <div className="container-fotito">
      <div className="foto">
        <img src={yo} alt=""/>
      </div>
      <div className="name">
        <h2>Angie Harumi Perez Sanchez</h2>
      </div>
    </div>
  );
};