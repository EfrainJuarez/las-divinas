import photo from '../../../assets/assetsEfrain/profile.jpg';
import './photoEJM.css'

export const PhotoEJM = () => {
  return (
    <div className="container-photo">
        <img id="profile" src={photo} alt=""/>
    </div>
  );
};
