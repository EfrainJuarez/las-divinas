import photo from '../../../assets/assetsEfrain/pacific rim.jpg';
import './peliculaEJM.css'

export const PeliculaEJM = () => {
  return (
<div className="card">
    <div className="face front">Pelicula favorito 🎬 </div>
    <div className="face back">
    <img id="profile" src={photo} alt=""/>
        <h1 id="pacific">Pacific Rim</h1>
    </div>
</div>
  );
};
