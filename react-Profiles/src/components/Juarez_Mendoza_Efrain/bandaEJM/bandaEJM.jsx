import photo from '../../../assets/assetsEfrain/the lumineers.jpg';
import './bandaEJM.css'

export const BandaEJM = () => {
  return (
<div class="card">
    <div class="face front">Banda favorito 🎵 </div>
    <div class="face back">
        <img id="profile" src={photo} alt=""/>
        <h1 id="texto">The Lumineers</h1>
    </div>
</div>
  );
};
