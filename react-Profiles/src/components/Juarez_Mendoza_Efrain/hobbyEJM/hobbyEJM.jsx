import photo from '../../../assets/assetsEfrain/peliculas.jpeg';
import './hobbyEJM.css'

export const HobbyEJM = () => {
  return (
<div className="card">
    <div className="face front">Hobby🌟 </div>
    <div className="face back">
    <img id="profile" src={photo} alt=""/>
        <h1 id="texto">Peliculas/Series</h1>
    </div>
</div>
  );
};
