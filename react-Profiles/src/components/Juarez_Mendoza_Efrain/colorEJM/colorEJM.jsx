import photo from '../../../assets/assetsEfrain/azul.jpg';
import './colorEJM.css'

export const ColorEJM = () => {
  return (
<div className="card">
    <div className="face front">Color favorito 🌟 </div>
    <div className="face back">
    <img id="profile" src={photo} alt=""/>
        <h1 id="azul">Azul</h1>
    </div>
</div>
  );
};
