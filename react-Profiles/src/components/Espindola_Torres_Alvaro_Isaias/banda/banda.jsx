import albumBanda from '../../../assets/assetsAlvaro/easy-life.jpg';
import './banda.css'

export const Banda = () => {
  return (
    <div className="principal-container-band">
      <div className="banda-name">
        <h2>Easy Life</h2>
      </div>
      <div className="band">
        <img src={albumBanda} alt="" />
      </div>
    </div>
  );
};
