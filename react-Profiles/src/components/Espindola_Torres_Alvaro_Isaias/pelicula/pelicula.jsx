import portadaPelicula from '../../../assets/assetsAlvaro/underworld.jpg';
import './pelicula.css';

export const Pelicula = () => {
  return (
    <div className="principal-container-pelicula">
      <div className="pelicula-title">
        <h2>Película favorita</h2>
      </div>
      <div className="portada">
        <img src={portadaPelicula} alt="" />
      </div>
    </div>
  );
};
