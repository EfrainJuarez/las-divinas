import photo from '../../../assets/assetsAlvaro/alvaro.jpeg';
import './photo.css';

export const Photo = () => {
  return (
    <div className="container-photo">
      <div className="photo">
        <img src={photo} alt=""/>
      </div>
      <div className="name">
        <h2>Álvaro Isaías Espíndola Torres</h2>
      </div>
    </div>
  );
};
