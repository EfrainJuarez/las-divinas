import "./cancion.css";
import album from "../../../assets/assetsAlvaro/photoAlbum.jpg";
import playIco from "../../../assets/assetsAlvaro/play.png";
import pauseIco from "../../../assets/assetsAlvaro/pause.png";

export const Cancion = () => {

  const reproducirCancion = () =>{
    let icono = document.querySelector(".btn");
    if (icono?.classList.toggle("reproducir")) {
      icono?.setAttribute("src",pauseIco);
    }else{
      icono?.setAttribute("src",playIco);
    }
  }

  return (
    <div className="principal-container-song">
      <div className="photoAlbum">
        <img className="song" src={album} alt="" />
        <div className="botones">
          <img src={playIco} alt="" className="btn" onClick={reproducirCancion}/>
        </div>
      </div>
    </div>
  );
}
