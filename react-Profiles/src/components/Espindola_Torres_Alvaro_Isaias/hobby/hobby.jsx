import hobbyPhoto from '../../../assets/assetsAlvaro/dubov.jpeg';
import './hobby.css';

export const Hobby = () => {
  return (
    <div className="principal-container-hobby">
      <div className='hobby-title'>
        <h2>Hobby</h2>
      </div>
      <div className="hobby">
        <img src={hobbyPhoto} alt="" />
      </div>
    </div>
  );
};
